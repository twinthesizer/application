import React from 'react'

import { Link } from 'react-router-dom'

class FourOFour extends React.Component {

    state = {}

    render() {
        return (
            <div>
                <h1>Ooops!</h1>
                <h2>The requested element was not found! :(</h2>
                <span style={{ color: 'rgba(0,0,0,0.5)' }}>404</span>
            </div>
        )
    }

}

export default FourOFour