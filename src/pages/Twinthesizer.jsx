import React from 'react'

import io from 'socket.io-client'

import { Container, Row, Col, InputGroup, InputGroupAddon, Button, Input, ButtonDropdown, InputGroupButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'

import logo from '../assets/imgs/twinthesizer.svg'
import anger from '../assets/imgs/anger.svg'
import disgust from '../assets/imgs/disgust.svg'
import fear from '../assets/imgs/fear.svg'
import joy from '../assets/imgs/joy.svg'
import sad from '../assets/imgs/sad.svg'

import { Tweetbox } from '../components/'

class Twinthesizer extends React.Component {

    state = {
        tweetId: '',
        hashtag: '',
        disabled: false
    }

    startStream() {
        console.log(this.state.hashtag)
        const socket = io()
        socket.emit('startStream', this.state.hashtag)
        this.setState({disabled: true})

        socket.on('playSound', data => {
            console.log('setting tweedid to ' + data.id)
            this.setState({tweetId: data.id})
        })
    }

    render() {
        console.log('rerender')
        console.log(this.state)
        return (
            <Container style={{ marginTop: 180 }}>
                <Row>
                    <Col sm='12'>
                        <h1 style={{ fontSize: 90 }}><img src={logo} height='80' />Twinthesizer</h1>
                    </Col>
                </Row>
                <Row style={{ marginTop: 20}}>
                    <Col sm='12'>
                        <InputGroup size='lg'>
                            <InputGroupAddon addonType='prepend'>#</InputGroupAddon>
                            <Input placeholder='Hashtag' value={this.state.hashtag} onChange={event => {this.setState({hashtag: event.target.value})}} />
                            <InputGroupAddon addonType='append'><Button disabled={this.state.disabled} outline color='primary' onClick={this.startStream.bind(this)}>Stream</Button></InputGroupAddon>
                        </InputGroup>
                    </Col>
                </Row>
                <Row style={{ marginTop: 20}}>
                    <Col sm='2'>
                        <img src={fear} height='80' />
                    </Col>
                    <Col sm='2'>
                        <img src={disgust} width='80' />
                    </Col>
                    <Col sm='2'>
                        <img src={anger} height='80' />
                    </Col>
                    <Col sm='2'>
                        <img src={sad} width='80' />
                    </Col>
                    <Col sm='2'>
                        <img src={joy} width='80' />
                        <label id="joy_perc"></label>
                    </Col>
                </Row>
                {
                    this.state.tweetId ? (
                        <Row style={{ marginTop: 80 }}>
                            <Col sm='12' md={{ size: 6, offset: 3 }}>
                                <Tweetbox id={this.state.tweetId} />
                            </Col>
                        </Row>
                    ) : (
                        <div>:(</div>
                    )
                }

            </Container>
        )
    }
}

export default Twinthesizer