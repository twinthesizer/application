import React from 'react'

import TweetEmbed from 'react-tweet-embed'

const Tweetbox = props => {

    console.log('rendereing tweet from tweetbox with id ' + props.id)

    return (
        <TweetEmbed id={props.id} />
    )

}

export default Tweetbox