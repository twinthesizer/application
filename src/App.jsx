import React from 'react'

import { BrowserRouter, Route, Link, Switch } from 'react-router-dom'

import { Twinthesizer, FourOFour } from './pages/'

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={Twinthesizer} />
                    <Route component={FourOFour} />
                </Switch>
            </BrowserRouter>
        )
    }
}

export default App